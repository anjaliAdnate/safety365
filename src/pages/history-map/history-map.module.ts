import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HistoryMapPage } from './history-map';

@NgModule({
  declarations: [
    HistoryMapPage,
  ],
  imports: [
    IonicPageModule.forChild(HistoryMapPage),
  ],
})
export class HistoryMapPageModule {}
