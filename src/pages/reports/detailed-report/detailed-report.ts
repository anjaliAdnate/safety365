import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ApiCallerProvider } from '../../../providers/api-caller/api-caller';
import * as moment from 'moment';
/**
 * Generated class for the DetailedReportPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-detailed-report',
  templateUrl: 'detailed-report.html',
})
export class DetailedReportPage implements OnInit {

  prePageData: any;
  isstudent: string;
  chidReport: any;
  datetimeStart: string;
  startDate: moment.Moment;
  endDate: moment.Moment;
  nameofmonth: any;
  childData: any = [];
  childName: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public apiCall: ApiCallerProvider) {
    this.prePageData = this.navParams.get('data');

    this.childData = navParams.get("param");
    

    this.childName=navParams.get('childName').name;
    console.log("data of child", JSON.stringify(this.childName))
    this.nameofmonth = this.childData.monthString;
    this.startDate = moment([this.childData.year, this.childData.month - 1]);
    this.endDate = moment(this.startDate).endOf('month');
    

    //  for (var i = 0; i < this.prePageData.statusReport.length; i++) {
    //     // this.nameofmonth= this.prePageData.statusReport[i].monthString
    //     // console.log("student data name",this.prePageData.statusReport[i].year);

    //     this.startDate = moment([this.prePageData.statusReport[i].year, this.prePageData.statusReport[i].month - 1]);

    //     // Clone the value before .endOf()
    //     this.endDate = moment(this.startDate).endOf('month');
    //     console.log("start",this.startDate.toISOString());
    //     console.log('end',this.endDate.toISOString());
    //   }



    // just for demonstration:

    // console.log("start",this.startDate);
    // console.log('end',this.endDate);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetailedReportPage');
  }

  ngOnInit() {
    this.ShowReportData();
  }

  ShowReportData() {
    var data =
    {
      "studentId": { "_eval": "Id", "value": this.prePageData.studentId },
      "start": { "_eval": "dayStart", "value": this.startDate.toISOString() },
      "end": { "_eval": "dayEnd", "value": this.endDate.toISOString()}
    }

    this.apiCall.startLoading().present();
    this.apiCall.ReportCall(data)
      .subscribe(data => {
        console.log("student report data", data);

        this.chidReport = data;
        console.log("student report", this.chidReport);

        this.apiCall.stopLoading();

        localStorage.setItem('student', data);
        this.isstudent = localStorage.getItem('student');
      },
        error => {
          this.apiCall.stopLoading();
          console.log(error);
        });

  }



}
