import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, Platform, ToastController } from 'ionic-angular';
import { ApiCallerProvider } from '../../providers/api-caller/api-caller';
import { GoogleMap, GoogleMaps, LatLng, GoogleMapsEvent, Geocoder, GeocoderResult } from '@ionic-native/google-maps';

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage  implements OnInit {
  map: GoogleMap;
  profilesStudent: any;
  profilesParent: any;
  loginDetails: any;
  token: string;
  deviceUUID: string;
  profilesParentname: any;
  profilesParentphone: any;
  profilesParentemail: any;
  location: any;
  mapElement: any;
  profilesParenterelation: any;
  allData:any={};
  mapid: any;
  markerlatlong: any;
  data: { "_id": any; "latLng": { "lat": any; "lng": any; }; };
  lattitude: string;
  longtitude: string;
  studentid: any;
  stdn: { "_id": any; "latLng": { "lat": string; "lng": string; }; };
  address_show: any;
  locationShow: any;
  addressofstudent: any;

  constructor(public navCtrl: NavController, public navParams: NavParams,
  public apicall: ApiCallerProvider,public alertCtrl:AlertController,public toastCtrl: ToastController,) {
    this.location = new LatLng(42.346903, -71.135101);

    console.log("login details=> " + localStorage.getItem('details'));
    this.loginDetails = JSON.parse(localStorage.getItem('details'));
    console.log("login _id=> " + this.loginDetails._id);

    this.token = localStorage.getItem("DEVICE_TOKEN");
    this.deviceUUID = localStorage.getItem("UUID");
    console.log("UUID=> ", this.deviceUUID)
  }
  ionViewDidLoad() {
    
  }

  ngOnInit() {
    this.getProfile();
  }


  getProfile() {

    this.apicall.startLoading().present();

    this.apicall.getprofileApi(this.loginDetails._id )
      .subscribe(data => {
        this.apicall.stopLoading();

        this.profilesStudent = data[0].data;
        console.log("profilesStudent",this.profilesStudent);
        this.profilesParentname = data[0]._id.first_name;
        this.profilesParentphone = data[0]._id.phone;
        this.profilesParentemail = data[0]._id.email;
        this.profilesParenterelation = data[0]._id.relation
        
        this.profilesParent = data[0]._id;
        console.log("profilesParent",this.profilesParent);


        for (var i=0; i<data[0].data.length; i++) {
             this.mapid= data[0].data[i].students.name.fname
             this.studentid= data[0].data[i].students._id
            console.log('data map student id', this.mapid);
            console.log('data student id', this.studentid);

            (function(that,studentid){
              that.allData.map = GoogleMaps.create(that.mapid, {
                camera: {
                  target: { lat: data[0].data[i].students.latLng.lat, lng: data[0].data[i].students.latLng.lng },
                  zoom: 16
                  
                }
              });
        
              // let marker: Marker = that.allData.map.addMarkerSync({
              //   title: "Current Location",
              //   // snippet: 'that plugin is awesome!',
              //   position: {
              //              lat: data[0].data[i].students.latLng.lat ,
              //              lng: data[0].data[i].students.latLng.lng
              //             },
              //   icon: 'blue',
              //   animation: GoogleMapsAnimation.BOUNCE
              // });
    
                console.log('lat',data[0].data[i].students.latLng.lat);
                console.log('lng',data[0].data[i].students.latLng.lng);
    
              that.allData.map.addMarker({
                title: 'Student position',
                icon: 'green',
                animation: 'DROP',
                draggable:true,
                position: {
                  lat: data[0].data[i].students.latLng.lat,
                  lng: data[0].data[i].students.latLng.lng
                }
              }) 
              .then(marker => {
                marker.on(GoogleMapsEvent.MARKER_DRAG_END,studentid)
                  .subscribe(() => {
                    // console.log('data map id', studentid)
                    // console.log()
                    that.markerlatlong = marker.getPosition();
                     
                    console.log("latt1",that.markerlatlong.lat);
                    console.log("long1",that.markerlatlong.lng);
                   
                     
                            Geocoder.geocode({
                              "position": {
                                lat: that.markerlatlong.lat,
                                lng: that.markerlatlong.lng
                              }
                            }).then((results: GeocoderResult[]) => {
                              if (results.length == 0) {
                                //not found
                                return null;
                              }
                              console.log('address=>',results[0]);
                              let address: any = [
                                results[0].subThoroughfare || "",
                                results[0].thoroughfare || "",
                                results[0].locality || "",
                                results[0].adminArea || "",
                                results[0].postalCode || "",
                                results[0].country || ""
                              ].join(", ");
                      
                              that.addressofstudent = results[0].extra.lines[0];
                              console.log("pickup location ",that.addressofstudent);
                             
                            that.sendAddress(that.addressofstudent,studentid,that.markerlatlong.lat,that.markerlatlong.lng);
                      
                            });
       
 

                    // var initLat = that.markerlatlong.lat;
                    // var initLng = that.markerlatlong.lng;
                    // console.log(initLat,initLng);
                 
                    //     var geocoder = new google.maps.Geocoder();
                    //     var latlng = new google.maps.LatLng(initLat, initLng);
                    
                    //     var request = {
                    //       latLng: latlng
                    //     };
                
                    //     geocoder.geocode(request, function (data, status) {
                    //       if (status == google.maps.GeocoderStatus.OK) {
                    //         if (data[0] != null) {
                    //           that.address_show = data[0].formatted_address;
                    //           // device.locationAddress = outerThis.address_show;
                    //           that.locationShow = that.address_show;
                              
                    //           console.log('locationShow',that.locationShow);
                          
                    
                    //         } else {
                    
                    //           that.address_show = data[0].formatted_address;
                            
                    //           console.log("address=>",that.address_show);
                    //         }
                    //       }
                        
                    
                    //     })
                    // localStorage.setItem("latt1",that.markerlatlong.lat);
                    // localStorage.setItem("long1",that.markerlatlong.lng);
    
                  //  console.log("student report data", data[0].data[i].students._id);
                   
                    // this.http.get('API URL').map(res => res.json()).subscribe(data => {
                    //   console.log(data);
                    // });
                    //  {
                    //    "_id":"",
                    //   "latLng":{"lat":18.605381368800092,"lng":73.77491540209962}
                    //  }
    
                    //
                    // this.lattitude = localStorage.getItem("latt1");
                    // this.longtitude = localStorage.getItem("long1");
                    // console.log("lattitude", this.lattitude);
                    // console.log("longtitude",this.longtitude);
          
                    // console.log("student report data", data[0].data[i].students._id);
                    // this.data =
                    // {
                    //   "_id": data[0].data[i].students._id,
                    //   "latLng":{"lat":this.lattitude,"lng":this.longtitude}
                    //   // "studentId": { "_eval": "Id", "value": this.prePageData.studentId },
                    //   // "start": { "_eval": "dayStart", "value": this.startDate.toISOString() },
                    //   // "end": { "_eval": "dayEnd", "value": this.endDate.toISOString()}
                    // }
                
                    // this.apicall.startLoading().present();
                    // this.apicall.studenEdit(data)
                    //   .subscribe(data => {
                    //     console.log("student report data", data);
                
                        
                
                    //     this.apicall.stopLoading();
                    //     const toast = this.toastCtrl.create({
                    //       message: 'Student location updated. !',
                    //       duration: 3000,
                    //       position: 'bottom'
                    //     })
                    //     toast.present();
                    //    this.getProfile();
                    //   },
                    //     error => {
                    //       this.apicall.stopLoading();
                    //       console.log(error);
                    //     });
                
                      console.log("dara not enablle3ewrfwere")
               
                  });
              });
            })(this,this.studentid)
         
         
      };
        

      for (var i=0; i<data[0].data.length; i++) {
        this.mapid= data[0].data[i].students.name.fname
        this.studentid= data[0].data[i].students.name.lname+data[0].data[i].students.name.fname
       console.log('data map id', this.mapid);
       console.log('data school map id', this.studentid);

      
         this.allData.map = GoogleMaps.create(this.studentid, {
           camera: {
             target: { lat: data[0].data[i].students.school.latLng.lat, lng: data[0].data[i].students.school.latLng.lng },
             zoom: 16
             
           }
         });

         this.allData.map.addMarker({
          title: 'School Location',
          icon: 'red',
          animation: 'DROP',
          draggable:true,
          position: {
            lat: data[0].data[i].students.school.latLng.lat,
            lng: data[0].data[i].students.school.latLng.lng
          }
        });
      }
     
        if (this.profilesStudent.length == 0) {
          let alert = this.alertCtrl.create({
            message: "No Data Found",
            buttons: ['OK']
          });
          alert.present();
        }

      }, error => {
        this.apicall.stopLoading();
        console.log(error);
      })
  }



sendAddress(address,ID,Lat,Long){
  var stdn =
  {
    "_id": ID,
    "latLng":{"lat":Lat,"lng":Long},
    "address":address
    // "studentId": { "_eval": "Id", "value": that.prePageData.studentId },
    // "start": { "_eval": "dayStart", "value": that.startDate.toISOString() },
    // "end": { "_eval": "dayEnd", "value": that.endDate.toISOString()}
  }
  this.apicall.startLoading().present();
  this.apicall.studenEdit(stdn)
    .subscribe(data => {
      console.log("student report data", data);

      this.apicall.stopLoading();
      const toast = this.toastCtrl.create({
        message: 'Student location updated. !',
        duration: 3000,
        position: 'bottom'
      })
      toast.present();
       this.getProfile();
    },
      error => {
        this.apicall.stopLoading();
        console.log(error);
      });

}


// doRefresh(refresher) {
//   console.log('Begin async operation', refresher);
//   this.getProfile();
//   refresher.complete();
// }
}
